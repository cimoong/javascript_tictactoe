// JavaScript Document
$(document).ready(function() {
	var x = "x"
	var o = "o"
	var count = 0;
	var o_win = 0;
	var x_win = 0;
	var scale = $("input[name='scale']:checked").val();
	var gametype =$("input[name='gametype']:checked").val();
 
	function buildList(){
		$('#game').empty();
		for(i = 1; i <= scale; i++)
		{
			for(j = 1; j <= scale; j++)
			{
				var li = $('<li>');
				li.addClass('btn span1').text('+').attr('id', 'x'+i+'j'+j).appendTo('#game');
			}
		}
	}
	buildList();

	$("input[name='scale']").change(function(){
		  scale = $("input[name='scale']:checked").val();
		  $("#wrapper").removeClass();
		  $("#wrapper").addClass("span"+(parseInt(scale)+1));
		  buildList();
		  $("#reset").click();
	  });

	$("input[name='gametype']").change(function(){
		  gametype = $("input[name='gametype']:checked").val();
		  $("#reset").click();
	  });
	  
	$('#game').on('click','li',function(){
	  if ($(this).hasClass('disable'))
	  {
		alert('Already selected')
	  }
	  else if (count%2 == 0)
	  {
		  count++
		  $(this).text(o)
		  $(this).addClass('disable o btn-primary')
			checkWinner(o);
			if(count != 0){
				if(gametype == 'computer')
					computerMove();
			}
	  }
	   else  
	  {
		if(gametype == 'player')
		{
			count++
			$(this).text(x)
			$(this).addClass('disable x btn-info')
			checkWinner(x);
		}
	  }
	});
   
    $("#reset").click(function () {
		$("#game li").text("+");
		$("#game li").removeClass('disable')
		$("#game li").removeClass('o')
		$("#game li").removeClass('x')
		$("#game li").removeClass('btn-primary')
		$("#game li").removeClass('btn-info')
		count = 0
	});
	
	function computerMove(){
		var lis = document.getElementById("game").getElementsByTagName("li");
		var limit = scale * scale;
		var randomIndex = Math.floor(Math.random() * limit);
		var selectedMove = lis[randomIndex];
		if($(selectedMove).hasClass('disable'))
		{
			computerMove();
		}else
		{
			$(selectedMove).text(x)
			$(selectedMove).addClass('disable x btn-info')
			count++;
			checkWinner(x);
		}
	}
	
	function checkStepCount(){
		var lastCount = scale * scale;
		if(count == lastCount)
		{
			alert('Its a tie. It will restart.')
			$("#game li").text("+");
			$("#game li").removeClass('disable')
			$("#game li").removeClass('o')
			$("#game li").removeClass('x')
			$("#game li").removeClass('btn-primary')
			$("#game li").removeClass('btn-info')
			count = 0
		}
	}
	
	function checkWinner(t){
		if(scale == 3){
			checkStepCount();
			for(var i = 1; i <= scale; i++){
				if($("#x"+i+"j1").hasClass(t) && $("#x"+i+"j2").hasClass(t) && $("#x"+i+"j3").hasClass(t)) //horizontal
				{
					alert(t+' wins')
					count = 0
					if(t == 'x'){
						x_win++
						$('#x_win').text(x_win)
						break;
					}else{
						o_win++
						$('#o_win').text(o_win)
						break;
					}
				}
				else if($("#x1j"+i).hasClass(t) && $("#x2j"+i).hasClass(t) && $("#x3j"+i).hasClass(t)) //vertical
				{
					alert(t+' wins')
					count = 0
					if(t == 'x'){
						x_win++
						$('#x_win').text(x_win)
						break;
					}else{
						o_win++
						$('#o_win').text(o_win)
						break;
					}
				}
				else if($("#x1j1").hasClass(t) && $("#x2j2").hasClass(t) && $("#x3j3").hasClass(t) //cross
						||$("#x1j3").hasClass(t) && $("#x2j2").hasClass(t) && $("#x3j1").hasClass(t)
						)
				{
					alert(t+' wins')
					count = 0
					if(t == 'x'){
						x_win++
						$('#x_win').text(x_win)
						break;
					}else{
						o_win++
						$('#o_win').text(o_win)
						break;
					}
				}
			}
		}
		else if(scale == 5)
		{
			checkStepCount();
			for(var i = 1; i <= scale; i++){
				if($("#x"+i+"j1").hasClass(t) && $("#x"+i+"j2").hasClass(t) && $("#x"+i+"j3").hasClass(t) && $("#x"+i+"j4").hasClass(t) && $("#x"+i+"j5").hasClass(t)) //horizontal
				{
					alert(t+' wins')
					count = 0
					if(t == 'x'){
						x_win++
						$('#x_win').text(x_win)
						break;
					}else{
						o_win++
						$('#o_win').text(o_win)
						break;
					}
				}
				else if($("#x1j"+i).hasClass(t) && $("#x2j"+i).hasClass(t) && $("#x3j"+i).hasClass(t) && $("#x4j"+i).hasClass(t) && $("#x5j"+i).hasClass(t)) //vertical
				{
					alert(t+' wins')
					count = 0
					if(t == 'x'){
						x_win++
						$('#x_win').text(x_win)
						break;
					}else{
						o_win++
						$('#o_win').text(o_win)
						break;
					}
				}
				else if($("#x1j1").hasClass(t) && $("#x2j2").hasClass(t) && $("#x3j3").hasClass(t) && $("#x4j4").hasClass(t) && $("#x5j5").hasClass(t) //cross
						||$("#x1j5").hasClass(t) && $("#x2j4").hasClass(t) && $("#x3j3").hasClass(t) && $("#x4j2").hasClass(t) && $("#x5j1").hasClass(t)
						)
				{
					alert(t+' wins')
					count = 0
					if(t == 'x'){
						x_win++
						$('#x_win').text(x_win)
						break;
					}else{
						o_win++
						$('#o_win').text(o_win)
						break;
					}
				}
			}
			
		}
		else if(scale == 7)
		{
			checkStepCount();
			for(var i = 1; i <= scale; i++){
				if($("#x"+i+"j1").hasClass(t) && $("#x"+i+"j2").hasClass(t) && $("#x"+i+"j3").hasClass(t) && $("#x"+i+"j4").hasClass(t) && $("#x"+i+"j5").hasClass(t) && $("#x"+i+"j6").hasClass(t) && $("#x"+i+"j7").hasClass(t)) //horizontal
				{
					alert(t+' wins')
					count = 0
					if(t == 'x'){
						x_win++
						$('#x_win').text(x_win)
						break;
					}else{
						o_win++
						$('#o_win').text(o_win)
						break;
					}
				}
				else if($("#x1j"+i).hasClass(t) && $("#x2j"+i).hasClass(t) && $("#x3j"+i).hasClass(t) && $("#x4j"+i).hasClass(t) && $("#x5j"+i).hasClass(t) && $("#x6j"+i).hasClass(t) && $("#x7j"+i).hasClass(t)) //vertical
				{
					alert(t+' wins')
					count = 0
					if(t == 'x'){
						x_win++
						$('#x_win').text(x_win)
						break;
					}else{
						o_win++
						$('#o_win').text(o_win)
						break;
					}
				}
				else if($("#x1j1").hasClass(t) && $("#x2j2").hasClass(t) && $("#x3j3").hasClass(t) && $("#x4j4").hasClass(t) && $("#x5j5").hasClass(t) && $("#x6j6").hasClass(t) && $("#x7j7").hasClass(t) //cross
						||$("#x1j7").hasClass(t) && $("#x2j6").hasClass(t) && $("#x3j5").hasClass(t) && $("#x4j4").hasClass(t) && $("#x5j3").hasClass(t) && $("#x6j2").hasClass(t) && $("#x7j1").hasClass(t)
						)
				{
					alert(t+' wins')
					count = 0
					if(t == 'x'){
						x_win++
						$('#x_win').text(x_win)
						break;
					}else{
						o_win++
						$('#o_win').text(o_win)
						break;
					}
				}
			}
			
		}
		else if(scale == 11)
		{
			checkStepCount();
			for(var i = 1; i <= scale; i++){
				if($("#x"+i+"j1").hasClass(t) && $("#x"+i+"j2").hasClass(t) && $("#x"+i+"j3").hasClass(t) && $("#x"+i+"j4").hasClass(t) && $("#x"+i+"j5").hasClass(t) && $("#x"+i+"j6").hasClass(t) && $("#x"+i+"j7").hasClass(t) && $("#x"+i+"j8").hasClass(t) && $("#x"+i+"j9").hasClass(t) && $("#x"+i+"j10").hasClass(t) && $("#x"+i+"j11").hasClass(t))//horizontal
				{
					alert(t+' wins')
					count = 0
					if(t == 'x'){
						x_win++
						$('#x_win').text(x_win)
						break;
					}else{
						o_win++
						$('#o_win').text(o_win)
						break;
					}
				}
				else if($("#x1j"+i).hasClass(t) && $("#x2j"+i).hasClass(t) && $("#x3j"+i).hasClass(t) && $("#x4j"+i).hasClass(t) && $("#x5j"+i).hasClass(t) && $("#x6j"+i).hasClass(t) && $("#x7j"+i).hasClass(t) && $("#x8j"+i).hasClass(t) && $("#x9j"+i).hasClass(t) && $("#x10j"+i).hasClass(t) && $("#x11j"+i).hasClass(t)) //vertical
				{
					alert(t+' wins')
					count = 0
					if(t == 'x'){
						x_win++
						$('#x_win').text(x_win)
						break;
					}else{
						o_win++
						$('#o_win').text(o_win)
						break;
					}
				}
				else if($("#x1j1").hasClass(t) && $("#x2j2").hasClass(t) && $("#x3j3").hasClass(t) && $("#x4j4").hasClass(t) && $("#x5j5").hasClass(t) && $("#x6j6").hasClass(t) && $("#x7j7").hasClass(t) && $("#x8j8").hasClass(t) && $("#x9j9").hasClass(t) && $("#x10j10").hasClass(t) && $("#x11j11").hasClass(t)//cross
						||$("#x1j11").hasClass(t) && $("#x2j10").hasClass(t) && $("#x3j9").hasClass(t) && $("#x4j8").hasClass(t) && $("#x5j7").hasClass(t) && $("#x6j6").hasClass(t) && $("#x7j5").hasClass(t) && $("#x8j4").hasClass(t) && $("#x9j3").hasClass(t) && $("#x10j2").hasClass(t) && $("#x11j1").hasClass(t)
						)
				{
					alert(t+' wins')
					count = 0
					if(t == 'x'){
						x_win++
						$('#x_win').text(x_win)
						break;
					}else{
						o_win++
						$('#o_win').text(o_win)
						break;
					}
				}
			}
		}
	}
});
